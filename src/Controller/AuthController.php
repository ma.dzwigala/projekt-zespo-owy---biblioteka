<?php

namespace App\Controller;

use App\Entity\User;
use App\Request\RegisterRequest;
use App\Utils\FormErrors;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class AuthController extends AbstractController
{
    /**
     * @Route(methods={"POST"}, path="/register", name="user_register")
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $form = $this->createForm(RegisterRequest::class, new User());
        $form->submit(json_decode($request->getContent(), true));
        if (!$form->isValid()) {
            return new JsonResponse((new FormErrors())->getArray($form), 422);
        }
        /** @var User $data */
        $data = $form->getData();
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new JsonResponse([], 201);
    }

    /**
     * @Route(methods={"POST"}, path="/login", name="user_login")
     * @param Request $request
     * @param AuthenticationSuccessHandler $handler
     * @param EncoderFactoryInterface $encoderFactory
     * @return JsonResponse
     */
    public function login(Request $request, AuthenticationSuccessHandler $handler, EncoderFactoryInterface $encoderFactory): JsonResponse
    {
        $requestData = json_decode($request->getContent(), true);
        $login = $requestData['email'] ?? null;
        $password = $requestData['password'] ?? null;
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->findOneBy(['email' => $login]);
        if ($user === null) {
            return new JsonResponse(['message' => 'Incorrect data'], 422);
        }
        $encoder = $encoderFactory->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            return new JsonResponse(['message' => 'Incorrect data'], 422);
        }
        $successAuthHandler = $handler;
        return $successAuthHandler->handleAuthenticationSuccess($user);
    }
}
