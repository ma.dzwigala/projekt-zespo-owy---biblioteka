<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\VirtualBookAccess;
use App\Services\User\UserContext;
use Doctrine\ORM\EntityManagerInterface;

class VirtualBookAccessDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserContext
     */
    private $userContext;

    public function __construct(EntityManagerInterface $entityManager, UserContext $userContext)
    {
        $this->entityManager = $entityManager;
        $this->userContext = $userContext;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return VirtualBookAccess::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null)
    {
        $user = $this->userContext->getUser();
        $isAdmin = $user->isAdmin();
        $repo = $this->entityManager->getRepository(VirtualBookAccess::class);
        if ($isAdmin) {
            return $repo->findAll();
        }
        return $repo->getOfUser($user);
    }
}
