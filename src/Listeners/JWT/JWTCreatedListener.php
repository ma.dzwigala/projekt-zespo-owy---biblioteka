<?php

namespace App\Listeners\JWT;

use App\Entity\User;
use App\Services\User\UserContext;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener
{
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $payload = $event->getData();
        /** @var User $user */
        $user = $event->getUser();
        $payload['id'] = $user->getId();
        $event->setData($payload);
    }
}
