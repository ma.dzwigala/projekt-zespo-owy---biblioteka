<?php

namespace App\Request;

use App\Entity\User;
use App\Services\LibraryCardNumber\LibraryCardNumberGeneratorInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterRequest extends AbstractType
{
    /**
     * @var LibraryCardNumberGeneratorInterface
     */
    private $cardNumberGenerator;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(LibraryCardNumberGeneratorInterface $cardNumberGenerator, UserPasswordEncoderInterface $encoder)
    {
        $this->cardNumberGenerator = $cardNumberGenerator;
        $this->encoder = $encoder;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('password', PasswordType::class);
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var User $user */
            $user = $event->getData();
            $user->setLibraryCardNumber($this->cardNumberGenerator->generate());
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'csrf_protection' => false
            ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
