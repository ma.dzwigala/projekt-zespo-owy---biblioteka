<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191201165457 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE person_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE book_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE borrowing_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE book_instance_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE virtual_book_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE virtual_book_access_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE person (id INT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, birth_date DATE NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE book (id INT NOT NULL, title VARCHAR(255) NOT NULL, isbn VARCHAR(255) NOT NULL, number_of_pages INT NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE persons_books (book_id INT NOT NULL, person_id INT NOT NULL, PRIMARY KEY(book_id, person_id))');
        $this->addSql('CREATE INDEX IDX_549BA55016A2B381 ON persons_books (book_id)');
        $this->addSql('CREATE INDEX IDX_549BA550217BBB47 ON persons_books (person_id)');
        $this->addSql('CREATE TABLE book_tag (book_id INT NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(book_id, tag_id))');
        $this->addSql('CREATE INDEX IDX_F2F4CE1516A2B381 ON book_tag (book_id)');
        $this->addSql('CREATE INDEX IDX_F2F4CE15BAD26311 ON book_tag (tag_id)');
        $this->addSql('CREATE TABLE app_user (id INT NOT NULL, salt VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, library_card_number VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE borrowing (id INT NOT NULL, book_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, borrowed_date DATE NOT NULL, expected_return_date DATE NOT NULL, return_date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_226E589716A2B381 ON borrowing (book_id)');
        $this->addSql('CREATE INDEX IDX_226E58977E3C61F9 ON borrowing (owner_id)');
        $this->addSql('CREATE TABLE book_instance (id INT NOT NULL, book_id INT DEFAULT NULL, in_stock BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5BCFA78716A2B381 ON book_instance (book_id)');
        $this->addSql('CREATE TABLE virtual_book (id INT NOT NULL, book_id INT DEFAULT NULL, source_uri VARCHAR(255) NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_226DBE0916A2B381 ON virtual_book (book_id)');
        $this->addSql('CREATE TABLE tag (id INT NOT NULL, value VARCHAR(255) NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE virtual_book_access (id INT NOT NULL, virtual_book_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, access_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, access_end TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_219B2F4820D5604E ON virtual_book_access (virtual_book_id)');
        $this->addSql('CREATE INDEX IDX_219B2F487E3C61F9 ON virtual_book_access (owner_id)');
        $this->addSql('ALTER TABLE persons_books ADD CONSTRAINT FK_549BA55016A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE persons_books ADD CONSTRAINT FK_549BA550217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_tag ADD CONSTRAINT FK_F2F4CE1516A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_tag ADD CONSTRAINT FK_F2F4CE15BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE borrowing ADD CONSTRAINT FK_226E589716A2B381 FOREIGN KEY (book_id) REFERENCES book_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE borrowing ADD CONSTRAINT FK_226E58977E3C61F9 FOREIGN KEY (owner_id) REFERENCES app_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_instance ADD CONSTRAINT FK_5BCFA78716A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE virtual_book ADD CONSTRAINT FK_226DBE0916A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE virtual_book_access ADD CONSTRAINT FK_219B2F4820D5604E FOREIGN KEY (virtual_book_id) REFERENCES virtual_book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE virtual_book_access ADD CONSTRAINT FK_219B2F487E3C61F9 FOREIGN KEY (owner_id) REFERENCES app_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE persons_books DROP CONSTRAINT FK_549BA550217BBB47');
        $this->addSql('ALTER TABLE persons_books DROP CONSTRAINT FK_549BA55016A2B381');
        $this->addSql('ALTER TABLE book_tag DROP CONSTRAINT FK_F2F4CE1516A2B381');
        $this->addSql('ALTER TABLE book_instance DROP CONSTRAINT FK_5BCFA78716A2B381');
        $this->addSql('ALTER TABLE virtual_book DROP CONSTRAINT FK_226DBE0916A2B381');
        $this->addSql('ALTER TABLE borrowing DROP CONSTRAINT FK_226E58977E3C61F9');
        $this->addSql('ALTER TABLE virtual_book_access DROP CONSTRAINT FK_219B2F487E3C61F9');
        $this->addSql('ALTER TABLE borrowing DROP CONSTRAINT FK_226E589716A2B381');
        $this->addSql('ALTER TABLE virtual_book_access DROP CONSTRAINT FK_219B2F4820D5604E');
        $this->addSql('ALTER TABLE book_tag DROP CONSTRAINT FK_F2F4CE15BAD26311');
        $this->addSql('DROP SEQUENCE person_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE book_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE borrowing_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE book_instance_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE virtual_book_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE virtual_book_access_id_seq CASCADE');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE persons_books');
        $this->addSql('DROP TABLE book_tag');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE borrowing');
        $this->addSql('DROP TABLE book_instance');
        $this->addSql('DROP TABLE virtual_book');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE virtual_book_access');
    }
}
