<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Traits\BasicEntityAttributes;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     itemOperations={
 *       "get",
 *       "put"={"security"="is_granted('ROLE_ADMIN')"},
 *     }
 * )
 */
class Book
{
    use BasicEntityAttributes;
    /**
     * @var Person[]|Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", inversedBy="books")
     * @ORM\JoinTable(name="persons_books")
     * @ApiSubresource()
     */
    private $authors;
    /**
     * @var string $title
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $isbn;
    /**
     * @var string
     * @ORM\Column(type="integer")
     */
    private $numberOfPages;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var BookInstance[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\BookInstance", mappedBy="book")
     */
    private $instances;

    /**
     * @var Tag[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag")
     */
    private $tags;

    /**
     * @var VirtualBook|null
     * @ApiSubresource()
     * @ORM\OneToOne(targetEntity="App\Entity\VirtualBook", mappedBy="book", cascade={"persist"})
     */
    private $virtualBook;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->instances = new ArrayCollection();
        $this->authors = new ArrayCollection();
        $virtualBook = new VirtualBook();
        $virtualBook->setBook($this);
        $virtualBook->setEnabled(true);
        $virtualBook->setSourceUri('http://google.com/');
        $this->setVirtualBook(
            $virtualBook
        );
    }

    /**
     * @return VirtualBook|null
     */
    public function getVirtualBook(): ?VirtualBook
    {
        return $this->virtualBook;
    }

    /**
     * @param VirtualBook|null $virtualBook
     */
    public function setVirtualBook(?VirtualBook $virtualBook): void
    {
        $this->virtualBook = $virtualBook;
    }

    /**
     * @return Person[]|Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param Person[]|Collection $authors
     */
    public function setAuthors($authors): void
    {
        $this->authors = $authors;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getIsbn(): string
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn(string $isbn): void
    {
        $this->isbn = $isbn;
    }

    /**
     * @return string
     */
    public function getNumberOfPages(): string
    {
        return $this->numberOfPages;
    }

    /**
     * @param string $numberOfPages
     */
    public function setNumberOfPages(string $numberOfPages): void
    {
        $this->numberOfPages = $numberOfPages;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return BookInstance[]|Collection
     */
    public function getInstances()
    {
        return $this->instances;
    }

    /**
     * @param BookInstance[]|Collection $instances
     */
    public function setInstances($instances): void
    {
        $this->instances = $instances;
    }
}
