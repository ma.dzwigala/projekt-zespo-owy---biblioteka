<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\BasicEntityAttributes;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     itemOperations={
 *       "get",
 *       "put"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 * )
 */
class Tag
{
    use BasicEntityAttributes;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $value;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
