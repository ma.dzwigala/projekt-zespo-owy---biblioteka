<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\BasicEntityAttributes;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     itemOperations={
 *       "get",
 *       "put"={"security"="is_granted('ROLE_ADMIN')"},
 *     }
 * )
 */
class Person
{
    use BasicEntityAttributes;
    /**
     * @var string $firstName
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @var string $lastName
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var Book[]|Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Book", mappedBy="authors")
     */
    private $books;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    public function __construct(string $firstName, string $lastName)
    {
        $this->books = new ArrayCollection();
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return Book[]|Collection
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * @param Book[]|Collection $books
     */
    public function setBooks($books): void
    {
        $this->books = $books;
    }

    /**
     * @return DateTime
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime $birthDate
     */
    public function setBirthDate(DateTime $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
