<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Contracts\OwnerAwareInterface;
use App\Entity\Traits\BasicEntityAttributes;
use App\Entity\Traits\HasOwner;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Borrowing
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post",
 *     },
 *     itemOperations={
 *       "put"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *       "get"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"}
 *     }
 * )
 */
class Borrowing implements OwnerAwareInterface
{
    use HasOwner, BasicEntityAttributes;

    /**
     * @var BookInstance $book
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\BookInstance", inversedBy="borrowings")
     */
    private $book;

    /**
     * @var \DateTime $borrowedDate
     * @Assert\NotNull()
     * @ORM\Column(type="date")
     */
    private $borrowedDate;

    /**
     * @var \DateTime $expectedReturnDate|null
     * @ORM\Column(type="date")
     */
    private $expectedReturnDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="date")
     */
    private $returnDate;

    /**
     * @return BookInstance
     */
    public function getBook(): BookInstance
    {
        return $this->book;
    }

    /**
     * @param BookInstance $book
     */
    public function setBook(BookInstance $book): void
    {
        $this->book = $book;
    }

    /**
     * @return \DateTime
     */
    public function getBorrowedDate(): \DateTime
    {
        return $this->borrowedDate;
    }

    /**
     * @param \DateTime $borrowedDate
     */
    public function setBorrowedDate(\DateTime $borrowedDate): void
    {
        $this->borrowedDate = $borrowedDate;
    }

    /**
     * @return \DateTime
     */
    public function getExpectedReturnDate(): \DateTime
    {
        return $this->expectedReturnDate;
    }

    /**
     * @param \DateTime $expectedReturnDate
     */
    public function setExpectedReturnDate(\DateTime $expectedReturnDate): void
    {
        $this->expectedReturnDate = $expectedReturnDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getReturnDate(): ?\DateTime
    {
        return $this->returnDate;
    }

    /**
     * @param \DateTime|null $returnDate
     */
    public function setReturnDate(?\DateTime $returnDate): void
    {
        $this->returnDate = $returnDate;
    }
}
