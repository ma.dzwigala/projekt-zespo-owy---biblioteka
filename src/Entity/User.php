<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Traits\BasicEntityAttributes;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_user")
 * @UniqueEntity(fields={"email"})
 * @ApiResource(
 *     collectionOperations={
 *       "get"={"security"="is_granted('ROLE_ADMIN')"},
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     itemOperations={
 *       "get"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *       "put"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *     },
 *     normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}}
 * )
 */
class User implements UserInterface
{
    use BasicEntityAttributes;

    /**
     * @var int $id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     * @ORM\Id()
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @var string
     * @ORM\Column(type="string", name="salt")
     */
    private $salt = '';

    /**
     * @var string
     * @Groups({"user:read", "user:write"})
     * @Assert\Email()
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $email;
    /**
     * @var string
     * @Groups({"user:write"})
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @Groups({"user:read", "user:write"})
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @Groups({"user:read", "user:write"})
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     * @Groups({"user:read"})
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    /**
     * @var string
     * @Groups({"user:read"})
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $libraryCardNumber;

    /**
     * @var VirtualBookAccess[]|Collection
     * @Groups({"user:read"})
     * @ApiSubresource()
     * @ORM\OneToMany(targetEntity="App\Entity\VirtualBookAccess", mappedBy="owner")
     */
    private $virtualBookAccesses;

    /**
     * @var bool
     * @Groups({"user:read"})
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isAdmin = false;

    public function __construct()
    {
        $this->virtualBookAccesses = new ArrayCollection();
    }

    public function getRoles()
    {
        return $this->isAdmin() ? ['ROLE_ADMIN'] : ['ROLE_USER'];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLibraryCardNumber(): ?string
    {
        return $this->libraryCardNumber;
    }

    /**
     * @param string $libraryCardNumber
     */
    public function setLibraryCardNumber(string $libraryCardNumber): void
    {
        $this->libraryCardNumber = $libraryCardNumber;
    }

    public function setPassword(string $password): void
    {
        $this->password= $password;
    }

    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function getVirtualBookAccesses(): ArrayCollection
    {
        return $this->virtualBookAccesses;
    }
}
