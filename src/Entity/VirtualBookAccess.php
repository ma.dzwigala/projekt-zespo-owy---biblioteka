<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Contracts\OwnerAwareInterface;
use App\Entity\Traits\BasicEntityAttributes;
use App\Entity\Traits\HasOwner;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\Repository\VirtualBookAccessRepository")
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post"
 *     },
 *     itemOperations={
 *       "put"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *       "get"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"}
 *     }
 * )
 */
class VirtualBookAccess implements OwnerAwareInterface
{
    use HasOwner, BasicEntityAttributes;

    /**
     * @var VirtualBook
     * @ORM\ManyToOne(targetEntity="App\Entity\VirtualBook")
     */
    private $virtualBook;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $accessStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $accessEnd;

    /**
     * @return VirtualBook
     */
    public function getVirtualBook(): VirtualBook
    {
        return $this->virtualBook;
    }

    /**
     * @param VirtualBook $virtualBook
     */
    public function setVirtualBook(VirtualBook $virtualBook): void
    {
        $this->virtualBook = $virtualBook;
    }

    /**
     * @return \DateTime
     */
    public function getAccessStart(): \DateTime
    {
        return $this->accessStart;
    }

    /**
     * @param \DateTime $accessStart
     */
    public function setAccessStart(\DateTime $accessStart): void
    {
        $this->accessStart = $accessStart;
    }

    /**
     * @return \DateTime
     */
    public function getAccessEnd(): \DateTime
    {
        return $this->accessEnd;
    }

    /**
     * @param \DateTime $accessEnd
     */
    public function setAccessEnd(\DateTime $accessEnd): void
    {
        $this->accessEnd = $accessEnd;
    }
}
