<?php

namespace App\Entity\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class VirtualBookAccessRepository extends EntityRepository
{
    public function getOfUser(User $user): array
    {
        return $this->findBy(['owner' => $user]);
    }
}
