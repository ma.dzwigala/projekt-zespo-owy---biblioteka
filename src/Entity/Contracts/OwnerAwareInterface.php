<?php

namespace App\Entity\Contracts;

use App\Entity\User;

interface OwnerAwareInterface
{
    public function getOwner(): User;
}
