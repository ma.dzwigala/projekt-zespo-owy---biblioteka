<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait BasicEntityAttributes
{
    /**
     * @var int $id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @ORM\Id()
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
