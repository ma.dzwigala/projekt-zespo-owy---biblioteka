<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\BasicEntityAttributes;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "get",
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *     },
 *     itemOperations={
 *       "get",
 *       "put"={"security"="is_granted('ROLE_ADMIN')"},
 *     }
 * )
 */
class BookInstance
{
    use BasicEntityAttributes;
    /**
     * @var Book
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Book", inversedBy="instances")
     */
    private $book;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $inStock;

    /**
     * @var Borrowing[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Borrowing", mappedBy="book")
     */
    private $borrowings;

    /**
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook(Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @return bool
     */
    public function isInStock(): bool
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock(bool $inStock): void
    {
        $this->inStock = $inStock;
    }

    /**
     * @return Borrowing[]|ArrayCollection
     */
    public function getBorrowings()
    {
        return $this->borrowings;
    }

    /**
     * @param Borrowing[]|ArrayCollection $borrowings
     */
    public function setBorrowings($borrowings): void
    {
        $this->borrowings = $borrowings;
    }
}
