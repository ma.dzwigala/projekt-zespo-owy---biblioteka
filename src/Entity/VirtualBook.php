<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\BasicEntityAttributes;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     collectionOperations={
 *       "post"={"security"="is_granted('ROLE_ADMIN')"},
 *       "get"
 *     },
 *     itemOperations={
 *       "put"={"security"="is_granted('ROLE_ADMIN')"},
 *       "get"
 *     },
 * )
 */
class VirtualBook
{
    use BasicEntityAttributes;
    /**
     * @var Book
     * @ORM\OneToOne(targetEntity="App\Entity\Book", inversedBy="virtualBook")
     */
    private $book;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $sourceUri;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook(Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @return string
     */
    public function getSourceUri(): string
    {
        return $this->sourceUri;
    }

    /**
     * @param string $sourceUri
     */
    public function setSourceUri(string $sourceUri): void
    {
        $this->sourceUri = $sourceUri;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
}
