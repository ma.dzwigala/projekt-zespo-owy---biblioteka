<?php

namespace App\Services\User;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserContext
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $storage)
    {
        $this->tokenStorage = $storage;
    }

    /**
     * @return User
     * @throws UserContextNotSet
     */
    public function getUser(): User
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            throw new UserContextNotSet();
        }
        /** @var User $user */
        $user = $token->getUser();
        if (is_string($user)) {
            throw new UserContextNotSet();
        }

        return $user;
    }
}
