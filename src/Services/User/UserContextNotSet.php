<?php

namespace App\Services\User;

use Throwable;

class UserContextNotSet extends \Exception
{
    public function __construct()
    {
        parent::__construct('UserContext not set');
    }
}
