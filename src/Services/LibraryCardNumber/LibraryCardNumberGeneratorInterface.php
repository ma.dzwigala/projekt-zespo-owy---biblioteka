<?php

namespace App\Services\LibraryCardNumber;

interface LibraryCardNumberGeneratorInterface
{
    public function generate(): string;
}
