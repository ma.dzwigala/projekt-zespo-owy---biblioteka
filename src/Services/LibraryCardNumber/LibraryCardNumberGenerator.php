<?php

namespace App\Services\LibraryCardNumber;

use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface as EntityManager;

class LibraryCardNumberGenerator implements LibraryCardNumberGeneratorInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    private const PREFIX = 'LCN';

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function generate(): string
    {
        $lastNumber = $this->getLastNumber();
        if ($lastNumber === null) {
            return $this->getNextNumber(0);
        }
        return $this->getNextNumber((int)str_replace(self::PREFIX, '', $lastNumber));
    }


    private function getLastNumber(): ?string
    {
        $conn = $this->entityManager->getConnection();
        $sql = <<<SQL
    SELECT library_card_number from "app_user"
    order by library_card_number desc limit 1
SQL;
        $statement = $conn->prepare($sql);
        $statement->execute();
        $result = $statement->fetchColumn();
        return $result !== false ? $result : null;
    }

    private function getNextNumber(int $number): string
    {
        $number++;
        $string = self::PREFIX;
        for ($i = 0; $i < 8 - strlen((string)$number); $i++) {
            $string .= '0';
        }
        $string .= (string)$number;
        return $string;
    }
}
