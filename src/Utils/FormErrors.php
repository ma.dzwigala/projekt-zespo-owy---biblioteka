<?php

namespace App\Utils;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

class FormErrors
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public function getArray(FormInterface $form)
    {
        return $this->getErrors($form);
    }

    /**
     * @param Form $form
     * @return string
     */
    public function getString(Form $form)
    {
        $errors = $this->getErrors($form);

        $string = "";

        foreach ($errors as $error) {
            if (is_array($error)) {
                $string .= implode(". ", $error) . ". ";
            } else {
                $string .= $error . ". ";
            }
        }

        return $string;

    }

    /**
     * @param $form
     * @return array
     */
    private function getErrors($form)
    {
        $errors = array();

        if ($form instanceof Form) {
            $i = 1;
            foreach ($form->getErrors() as $error) {
                $errors[$i] = $error->getMessage();
                $i++;
            }

            foreach ($form->all() as $key => $child) {
                /** @var $child Form */
                if ($err = $this->getErrors($child)) {
                    $errors[$key] = $err;
                }
            }
        }

        return $errors;
    }
}
